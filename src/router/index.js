import Vue from 'vue'
import Router from 'vue-router'
import CoCityWeather from '@/components/CoCityWeather'
import CoCityMusicEvents from '@/components/CoCityMusicEvents'
import CoContinents from '@/components/CoContinents'
import CoSubMenu from '@/components/CoSubMenu'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'menu',
      component: CoContinents
    },
    {
      path: '/:city',
      name: 'city',
      props: true,
      component: CoSubMenu,
      children: [
        {
          path: 'weather',
          name: 'weather',
          props: true,
          component: CoCityWeather
        },
        {
          path: 'events',
          name: 'events',
          props: true,
          component: CoCityMusicEvents
        }
      ]
    }
  ]
})

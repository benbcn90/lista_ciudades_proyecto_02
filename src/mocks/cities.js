export default [
  {
    america: [
      {'nombre': 'Lima'},
      {'nombre': 'Buenos Aires'},
      {'nombre': 'Los Angeles'},
      {'nombre': 'Bogotá'}
    ]
  },
  {
    europa: [
      {'nombre': 'Barcelona'},
      {'nombre': 'Lleida'},
      {'nombre': 'Sevilla'},
      {'nombre': 'Madrid'},
      {'nombre': 'Paris'},
      {'nombre': 'Amsterdam'}
    ]
  },
  {
    asia: [
      {'nombre': 'Pekin'},
      {'nombre': 'Tokyo'},
      {'nombre': 'Hong Konk'},
      {'nombre': 'Shangai'},
      {'nombre': 'Hanoi'}
    ]
  }
]

module.exports = {
  NODE_ENV: '"production"',
  API: '"https://api.openweathermap.org/data/2.5/"',
  API_MUSIC: '"https://api.songkick.com/api/3.0/search/locations.json"',
  API_MUSIC_EVENTS: '"https://api.songkick.com/api/3.0/metro_areas/"'
}
